﻿USE pesca;
-- indica el nombre del club que tiene pescadores
SELECT DISTINCT c.nombre FROM pescadores p JOIN clubes c ON p.club_cif = c.cif;

-- 2 indica el nombre del club que no tiene pescadores
  SELECT c1.nombre FROM clubes c1 LEFT JOIN pescadores p ON c1.cif = p.club_cif WHERE p.club_cif IS NULL;   

-- 3 indicar el nombre de los cotos autorizados a algún club
  SELECT DISTINCT a.coto_nombre FROM autorizados a;
  
-- 4 indica la provincia que tiene cotos autorizados a algún club
  SELECT DISTINCT c.provincia FROM autorizados a JOIN cotos c ON c.nombre= c.nombre;
  
-- 5 indica el nombre de los  cotos que no están autorizados a ningún club
  SELECT c.nombre FROM cotos c LEFT JOIN autorizados ON c.nombre = autorizados.coto_nombre WHERE coto_nombre IS NULL;
  
-- 6 indica el número de ríos por provincia con cotos
  SELECT COUNT(DISTINCT c.rio), c.provincia FROM cotos c GROUP BY c.provincia; 
  
-- 7 indica el número de ríos por provincia con cotos autorizados
  SELECT c.provincia, COUNT( DISTINCT c.rio) FROM cotos c JOIN autorizados a ON c.nombre = a.coto_nombre GROUP BY c.provincia; 
  
-- 8 indica el nombre de la provincia con más cotos autorizados
  SELECT COUNT( DISTINCT a.coto_nombre) numero, c.provincia  FROM autorizados a JOIN cotos c ON a.coto_nombre = c.nombre GROUP BY c.provincia;
  SELECT MAX( c1.numero)  FROM (SELECT COUNT( DISTINCT a.coto_nombre) numero, c.provincia  FROM autorizados a JOIN cotos c ON a.coto_nombre = c.nombre GROUP BY c.provincia) c1;
  SELECT c1.provincia FROM(SELECT COUNT( DISTINCT a.coto_nombre) numero, c.provincia  FROM autorizados a JOIN cotos c ON a.coto_nombre = c.nombre GROUP BY c.provincia) c1
    JOIN (SELECT MAX( c1.numero) maximo  FROM (SELECT COUNT( DISTINCT a.coto_nombre) numero, c.provincia  FROM autorizados a JOIN cotos c ON a.coto_nombre = c.nombre GROUP BY c.provincia) c1) c2
    ON c1.numero= c2.maximo;   
  
-- 9 indica el nombre del pescador y el nombre de su ahijado
  SELECT hijos.nombre nombreHijo,padre.nombre nombrePescador FROM pescadores hijos JOIN apadrinar a ON hijos.numSocio = a.ahijado JOIN pescadores padre ON a.padrino = padre.numSocio;
  
-- 10 indica el número de ahijados de cada pescador
  SELECT COUNT(*), a.padrino FROM apadrinar a GROUP BY a.padrino;                 
    



       